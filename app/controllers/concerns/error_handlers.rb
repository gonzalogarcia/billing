module ErrorHandlers
  extend ActiveSupport::Concern

  included do
    rescue_from ::StandardError, with: :default_error_handler
    rescue_from ActionController::RoutingError, with: :routing_error_handler 
    rescue_from BusinessError, with: :business_error_handler
    rescue_from ActiveRecord::RecordInvalid, with: :invalid_error_handler
  end

  def business_error_handler(error)
    respond_to do |format|
      format.json { render(json: { error: error.message }, status: 400) }
    end
  end

  def invalid_error_handler(error)
    rescue_as_business_error(error.message)
  end

  def default_error_handler(error)
    rescue_as_business_error("Ocurrio un error, por favor contactar al administrador.")
  end

  def rescue_as_business_error(msg)
    business_error_handler(BusinessError.new(msg))
  end

  def routing_error_handler(error)
    respond_to do |format|
      format.any { head :not_found }
    end
  end
end
