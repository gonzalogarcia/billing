class UsersController < ApplicationController
  before_action(:only_admin, only: [:create, :index, :destroy])
  before_action(:find_user, only: [:update, :show, :destroy])
  before_action(:user_can_edit, only: :update)

  def show
    respond_with(@user)
  end

  def update
    @user.update_attributes(user_params)
    respond_with(@user)
  end

  def index
    @users = User.all
    respond_with(@users)
  end

  def create
    @user = User.create(user_params)
    respond_with(@user)
  end

  def destroy
    raise BusinessError.new("El usuario #{@user.username} es un administrador.") if @user.is_admin
    @user.destroy!
    respond_with(@user)
  end

  private
    def user_params
      params.require(:user).permit(:username, :password, :password_confirmation)
    end

    def find_user
      @user = User.find(params[:id])
      raise BusinessError.new("El usuario no existe.") unless @user
    end

    def user_can_edit
      raise BusinessError.new("Usted no puede editar a #{@user.username}.") unless current_user.can_edit?(@user)
    end
end
