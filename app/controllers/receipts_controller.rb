class ReceiptsController < ApplicationController
  before_action(:validate_params, only: :create)
  before_action(:find_receipt, only: [:show, :destroy])

  def show
    respond_with(@receipt)
  end

  def destroy
    @receipt.cancel
    respond_with(@receipt)
  end

  def create
    @receipt = Receipt.create(
      client:  receipt_params[:partner] ? 
        Partner.find(receipt_params[:partner][:id]) : Client.find(receipt_params[:client][:id]), 
      date_of_issue: DateTime.now,
      full_amount: 0,
      legal_letter: receipt_params[:legal_letter],
      legal_place_code: receipt_params[:legal_place_code],
      legal_instance_code: receipt_params[:legal_instance_code])

    (receipt_params[:quota_lines] || []).each do |line|
      @receipt.receipt_lines.new(
        description: line[:description],
        type: QuotaReceiptLine.name,
        amount: line[:amount],
        quota: Quota.find(line[:quota][:id]),
        payment_method: line[:payment_method])
    end

    (receipt_params[:seminary_lines] || []).each do |line|
      @receipt.receipt_lines.new(
        type: SeminaryLine.name,
        amount: line[:amount],
        description: line[:description],
        seminary_id: line[:seminary_id],
        payment_method: line[:payment_method])
    end

    (receipt_params[:other_lines] || []).each do |line|
      @receipt.receipt_lines.new(
        description: line[:description],
        type: ReceiptLine.name,
        amount: line[:amount],
        payment_method: line[:payment_method])
    end

    unless @receipt.save
      @receipt.destroy
      raise BusinessError.new(@receipt.errors.messages.map{|k,v| v}.join)
    end

    respond_with(@receipt)
  end

  def index
    @receipts = Receipt.by_creation

    respond_with(@receipts)
  end

  def next_legal_code
    @last_legal_code = Receipt.next_legal_code

    respond_with(@last_legal_code)
  end

  def payment_methods
    @payment_methods = ReceiptLine.payment_methods_descriptions

    respond_with(@payment_methods)
  end

  def report
    @report = ReceiptsReport.new(report_params)

    respond_with(@report)
  end

  private
  def receipt_params
    params
  end

  def report_params
    params.permit(:from, :up_to)
  end

  def validate_params
    if (receipt_params[:quota_lines].blank? && receipt_params[:other_lines].blank? && receipt_params[:seminary_lines].blank?)
      raise BusinessError.new("No se puede hacer un recibo sin renglones.")
    end
  end

  def find_receipt
    @receipt = Receipt.find(params[:id])
  end
end
