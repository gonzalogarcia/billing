class PartnersController < ApplicationController
  before_action(:find_partner, only: [:show, :update, :receipts])
 
  def index
    Partner.generate_quotas
    @partners = Partner.all

    respond_with(@partner)
  end

  def show
    respond_with(@partner)
  end

  def update
    @partner.update(partner_params)
    respond_with(@partner)
  end


  def create
    @partner = Partner.create!(partner_params)
    
    respond_with(@partner)
  end

  def quotas
    @quotas = Partner.find(params[:partner_id]).quotas

    respond_with(@quotas)
  end

  def receipts
    @receipts = @partner.receipts
    respond_with(@receipts)
  end

  def coming_quota
    @partner = Partner.find(params[:id])
    @quota = @partner.coming_quota

    respond_with(@quota)
  end

  def overdue_quotas_report
    @overdue_quotas_report = OverdueQuotasReport.new(params[:minium_quotas_count].to_i || 0)

    respond_with(@overdue_quotas_report)
  end

  private
  def partner_params
    raise BusinessError.new("No se han incluido datos sobre el socio.") if params[:partner].blank?

    params.require(:partner).permit(
      :first_name, 
      :last_name,
      :cuit, 
      :email, 
      :actual_partner_category_id,
      :address,
      :place,
      :postal_code,
      :province,
      :identificator)
  end
 
  def find_partner
    @partner = Partner.find(params[:id])
  end
end
