class PartnerCategoriesController < ApplicationController
  before_action(:find_partner_category, only: [:show, :update])
  before_action(:only_admin, only: [:create, :update])

  def index
    @partner_categories = PartnerCategory.all
    respond_with(@partner_categories)
  end

  def show
    respond_with(@partner_category)
  end

  def partners
    @partners = PartnerCategory.find(params[:partner_category_id]).partners.order(:last_name, :first_name)
    respond_with(@partners)
  end

  def create
    @partner_category = PartnerCategory.create(partner_category_params)
    respond_with(@partner_category)
  end

  def update
    @partner_category.update(partner_category_params)
    respond_with(@partner_category)
  end

  private
  def find_partner_category
    @partner_category = PartnerCategory.find(params[:id])
  end

  def partner_category_params
    params.require(:partner_category).permit(:price, :description)
  end
end
