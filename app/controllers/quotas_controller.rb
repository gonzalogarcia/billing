class QuotasController < ApplicationController
  def next
    @quota = Quota.find(params[:id]).next

    respond_with(@quota)
  end
end
