class ApplicationController < ActionController::Base
  respond_to(:json)
  helper_method(:current_user)
  
  include ErrorHandlers

  before_action(:authenticate_user!, except: :by_username)

  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def authenticate_user!
    raise BusinessError.new("No se ha iniciado sesión") unless current_user.present?
  end

  def only_admin
    raise BusinessError.new("Usted no es administrador") unless current_user.is_admin
  end
end
