class ReceiptLinesController < ApplicationController
  before_action(:validate_review_params, only: [:index, :each_payment_review])
  before_action(:validate_index_params, only: :index)

  def index
    @receipt_lines = ReceiptLine.review(review_params)
    respond_with(@receipt_lines)
  end

  def each_payment_review
    @review_by_payments = ReceiptLineReviewByEachPayment.new(review_params[:from], review_params[:up_to])
    respond_with(@review_by_payments)
  end

  private
  def review_params
    params
  end

  def validate_review_params
    BusinessError.new("Falta especificar la fecha desde cuándo considerar.") unless review_params[:from].present?
    BusinessError.new("Falta especificar la fecha hasta cuándo considerar.") unless review_params[:up_to].present?
  end

  def validate_index_params
    BusinessError.new("Falta especificar el medio de pago.") unless review_params[:payment_method].present?
  end
end
