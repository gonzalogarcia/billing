class ClientsController < ApplicationController
  def seminary_offers
    @client = Client.find(params[:id])
    @seminary_offers = @client.seminary_offers

    respond_with(@seminary_offers)
  end
end
