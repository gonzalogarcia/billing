class SessionsController < ApplicationController
  skip_before_action(:authenticate_user!, except: :destroy)

  def create
    @user = User.authenticate(login_params[:username], login_params[:password])

    if @user
      session[:user_id] = @user.id
    else
      raise BusinessError.new("Datos de usuario inválidos.")
    end

    respond_with(@user)
  end

  def destroy
    @username = current_user.username
    session[:user_id] = nil
    respond_with(@username)
  end

  def logged_username
    @username = current_user.username if current_user

    respond_with(@username)
  end

  def is_admin
    @is_admin = current_user.is_admin if current_user

    respond_with(@is_admin)
  end

  private
  def login_params
    params.require(:session).permit(:username, :password)
  end
end
