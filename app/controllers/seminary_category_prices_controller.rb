class SeminaryCategoryPricesController < ApplicationController
  before_action(:find_seminary, only: [:index, :create])
  before_action(:only_admin, only: [:create])

  def index
    @seminary_category_prices = @seminary.seminary_category_prices
    respond_with(@seminary_category_prices)
  end

  def create
    @seminary_category_price = 
      @seminary.seminary_category_prices.find_by(partner_category_id: seminary_category_price_params[:partner_category_id]) || 
        @seminary.seminary_category_prices.new(partner_category_id: seminary_category_price_params[:partner_category_id])
    
    @seminary_category_price.price = seminary_category_price_params[:price]
    @seminary_category_price.save!
    respond_with(@seminary_category_price)
  end

  private
  def find_seminary
    @seminary = Seminary.find(params[:seminary_id])
  end

  def seminary_category_price_params
    params.require(:seminary_category_price).permit(:partner_category_id, :price)
  end
end
