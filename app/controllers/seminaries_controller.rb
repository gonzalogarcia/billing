class SeminariesController < ApplicationController
  before_action(:only_admin, except: [:show, :index])
  before_action(:find_seminary, only: [:show, :update])

  def index
    @seminaries = Seminary.by_held_on

    respond_with(@seminaries)
  end

  def show
    respond_with(@seminary)
  end

  def create
    @seminary = Seminary.new
    save_with_params
    respond_with(@seminary)
  end

  def update
    save_with_params
    respond_with(@seminary)
  end

  def not_defined_categories
    @seminary = Seminary.find(params[:seminary_id])
    @categories = @seminary.not_defined_categories
    respond_with(@categories)
  end

  private
  def find_seminary
    @seminary = Seminary.find(params[:id]) 
  end

  def seminary_params
    params.require(:seminary).permit(
      :title,
      :default_price)
  end

  def save_with_params
    @seminary.assign_attributes(
      title: seminary_params[:title],
      default_price: seminary_params[:default_price])
    @seminary.save!
  end
end
