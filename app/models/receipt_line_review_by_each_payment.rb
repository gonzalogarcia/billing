class ReceiptLineReviewByEachPayment
  attr_accessor(:from, :up_to)

  def initialize(from, up_to)
    self.from = from
    self.up_to = up_to
  end

  def reviews
    @reviews ||= ReceiptLine.payment_methods.map do |m, v| 
      ReceiptLineReview.new(PaymentMethod.new(m,v), self)
    end
  end

  def partner_categories_reviews
    @partner_categories_reviews ||= PartnerCategory.all.map { |c| PartnerCategoryReview.new(c, self) }
  end

  def size
    self.reviews.map(&:size).inject(0, :+)
  end

  def full_amount
    self.reviews.map(&:full_amount).inject(0, :+)
  end

  def quota_amount
    quota_lines.pluck(:amount).inject(0, :+)
  end

  def others_amount
    full_amount - quota_amount
  end

  def receipt_lines
    @receipt_lines ||= ReceiptLine
      .joins(:receipt)
      .between(from, up_to)
  end

  def quota_lines
    self.receipt_lines.where(type: QuotaReceiptLine.name)
  end
end
