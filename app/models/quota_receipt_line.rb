class QuotaReceiptLine < ReceiptLine
  after_save(:update_quota)
  after_destroy(:update_quota)
  validates_presence_of(:quota)
  
  def update_quota
    quota.update_paid_amount
  end
end
