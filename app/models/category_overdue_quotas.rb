class CategoryOverdueQuotas
  attr_accessor(:category, :minium_quotas_count)

  def initialize(category, minium_quotas_count)
    self.category = category
    self.minium_quotas_count = minium_quotas_count
  end

  def partners
    @partners ||= category.partners.by_full_name.select{|p| p.overdue_quotas_count >= minium_quotas_count }
  end

  def count
    partners.map(&:overdue_quotas_count).sum
  end

  def amount
    partners.map(&:overdue_quotas_amount).sum
  end

  def category_id
    category.id
  end

  def category_description
    category.description
  end
end
