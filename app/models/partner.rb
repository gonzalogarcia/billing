class Partner < Client
  belongs_to(:actual_partner_category, class_name: PartnerCategory.name)
  has_many(:quotas, -> { order(:year, :month) }, dependent: :destroy, foreign_key: :partner_id)
  
  validates_presence_of(:actual_partner_category)
 
  enum status: { active: 0, inactive: 1 }

  after_create(:generate_first_quota)
  after_save(:update_quotas)

  def paying
    self.actual_partner_category.paid?
  end

  def update_quotas
    quotas.upcoming.completely_unpaid.each { |q| q.change_category(actual_partner_category) }
  end

  def quota_created(y, m)
    self.update_attributes(last_quota_year: y, last_quota_month: m)
  end

  def generate_first_quota
    quotas.create!(year: Time.now.year, month: Time.now.month, partner_category: actual_partner_category)
  end

  def generate_quotas
    generate_first_quota if quotas.empty?

    last_quota_year = quotas.last.year
    last_quota_month = quotas.last.month
    if quotas.last.year < Time.now.year      
      (last_quota_month.next .. 12).to_a.each do |month|
        quotas.create!(month: month, year: last_quota_year, partner_category: actual_partner_category)
      end

      (last_quota_year.next .. Time.now.year.pred).to_a.each do |year|
        (1 .. 12).to_a.each do |month|
          quotas.create!(year: year, month: month, partner_category: actual_partner_category)
        end
      end
      
      (1..Time.now.month).to_a.each do |month|
        quotas.create!(year: Time.now.year, month: month, partner_category: actual_partner_category)
      end
    elsif quotas.last.year == Time.now.year
      (last_quota_month.next..Time.now.month).to_a.each do |month|
        quotas.create!(year: Time.now.year, month: month, partner_category: actual_partner_category)
      end
    end
  end

  def overdue_quotas
    quotas.overdue
  end

  def overdue_quotas_count
    overdue_quotas.length
  end

  def overdue_quotas_amount
    overdue_quotas.map(&:pending_amount).sum
  end

  def last_paid_quota
    quotas.paid.last.to_s
  end

  def coming_quota
    quotas.unpaid.any? ? quotas.unpaid.first : quotas.paid.last.next
  end

  def offer_to(seminary)
    seminary.partner_offer(self)
  end

  class << self
    def generate_quotas
      where("(clients.last_quota_year < ?) OR 
        ((clients.last_quota_year = ?) AND (clients.last_quota_month < ?))", 
        Time.now.year, Time.now.year, Time.now.month).each(&:generate_quotas)
    end

    def by_full_name
      order(:last_name, :first_name)
    end
  end
end
