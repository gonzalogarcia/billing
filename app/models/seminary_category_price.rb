class SeminaryCategoryPrice < ActiveRecord::Base
  belongs_to(:seminary)
  belongs_to(:partner_category)

  validates_presence_of(:seminary, :partner_category, :price)

  before_validation(:set_price)

  def partner_category_description
    partner_category.description
  end

  def seminary_title
    seminary.title
  end

  private
  def set_price
    self.price = seminary.default_price unless price
  end
end
