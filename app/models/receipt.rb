class Receipt < ActiveRecord::Base
  extend(WithDateOfIssue)

  belongs_to(:client, polymorphic: true)
  has_many(:receipt_lines, dependent: :destroy)

  validates_presence_of(:client, :date_of_issue, :full_amount)
  validates_presence_of(:legal_letter, :legal_place_code, :legal_instance_code)

  validates_numericality_of(:legal_place_code, in: 0..9999)
  validates_numericality_of(:legal_instance_code, in: 0..99999999)

  validates_uniqueness_of(:legal_instance_code, scope: [:legal_place_code, :legal_letter], 
    message: "Ya existe un recibo con tal número.")

  before_validation(:duplicate_client_data, on: :create)

  def payment_methods_amounts_description
    ReceiptLine.payment_methods
      .map{ |description, id| "#{description}: #{receipt_lines.where(payment_method: id).pluck(:amount).sum}" }
      .join('  ')
  end

  def legal_code_description
    "#{legal_letter}-#{replace_last_chars('0000', legal_place_code.to_s)}-#{replace_last_chars('00000000', legal_instance_code.to_s)}"
  end

  def replace_last_chars(base, replacer)
    base[(base.size - replacer.to_s.size)..-1] = replacer.to_s
    base
  end

  def cancel
    raise BusinessError.new(
      "No se puede cancelar un recibo mientras haya posteriores del mismo cliente.") unless self.last_client_receipt
    self.destroy!
  end

  def last_client_receipt
    client.receipts.last.id == id
  end

  def duplicate_client_data
    assign_attributes(
      client_full_name: client.full_name,
      client_address: client.address,
      client_place: client.place,
      client_postal_code: client.postal_code,
      client_province: client.province,
      client_cuit: client.cuit)
  end

  def date_of_issue_description
    date_of_issue.to_s
  end

  def date_of_issue_simple_description
    date_of_issue.strftime("%d/%m/%Y")
  end

  def client_place_with_postal_code
    "#{client_place} - #{client_postal_code}"
  end

  def client_full_name
    client.full_name
  end

  def update_full_amount
    update_attributes(full_amount: receipt_lines.pluck(:amount).sum)
  end

  class << self
    def by_creation
      order(:created_at)
    end

    def chronological_last
      by_creation.last
    end

    def next_legal_code
      return ReceiptLegalCode.default unless any?

      last_receipt = chronological_last

      ReceiptLegalCode.new(
        last_receipt.legal_letter,
        last_receipt.legal_place_code,
        last_receipt.legal_instance_code + 1)
    end
  end
end
