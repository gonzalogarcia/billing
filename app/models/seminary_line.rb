class SeminaryLine < ReceiptLine
  belongs_to(:seminary)
  validates_presence_of(:seminary)
end
