class PartnerCategoryReview
  attr_accessor(:partner_category, :receipt_line_review_by_each_payment)

  def initialize(partner_category, receipt_line_review_by_each_payment)
    self.partner_category = partner_category
    self.receipt_line_review_by_each_payment  = receipt_line_review_by_each_payment
  end

  def full_amount
    self
      .receipt_lines
      .pluck(:amount)
      .sum
  end

  def receipt_lines
    receipt_line_review_by_each_payment
      .receipt_lines
      .joins(:quota)
      .where("quota.partner_category_id = (?)", self.partner_category.id)
  end
end
