class PaymentMethod
  attr_accessor(:description, :id)
  
  def initialize(description, id)
    self.description = description
    self.id = id
  end
end
