class ReceiptLineReview
  attr_accessor(:receipt_line_review_by_each_payment, :payment_method)
  delegate(:size, to: :lines)
  delegate(:from, :up_to, to: :receipt_line_review_by_each_payment)

  def initialize(payment_method, receipt_line_review_by_each_payment)
    self.receipt_line_review_by_each_payment = receipt_line_review_by_each_payment
    self.payment_method = payment_method
  end

  def lines
    @lines ||= receipt_line_review_by_each_payment
      .receipt_lines
      .where(payment_method: payment_method.id)
  end

  def full_amount
    lines.pluck(:amount).sum
  end

  def payment_method_description
    payment_method.description
  end
end
