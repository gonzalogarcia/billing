class ReceiptLine < ActiveRecord::Base
  extend(WithDateOfIssue)

  belongs_to(:receipt)
  belongs_to(:quota)
  validates_presence_of(:receipt, :amount, :description, :payment_method)

  before_validation(:default_payment_method)
  after_create(:update_receipt)

  enum payment_method: { 
    cash: 0, 
    credit: 1, 
    debit: 2, 
    check: 3, 
    transfer: 4,
    pagomiscuentas: 5,
    bonification: 6 
  }

  delegate(:date_of_issue, :date_of_issue_simple_description, :client_full_name, :client_cuit, :client_id, to: :receipt)

  def update_receipt
    receipt.update_full_amount
  end

  def receipt_code
    receipt.legal_code_description
  end

  class <<self
    def payment_methods_descriptions
      payment_methods.map { |k, v| PaymentMethod.new(k, v) }
    end

    def review(params)
      joins(:receipt)
        .between(params[:from], params[:up_to])
        .where(payment_method: payment_methods[params[:payment_method]])
    end
  end

  private
  def default_payment_method
    self.payment_method = :chash unless payment_method
  end

end
