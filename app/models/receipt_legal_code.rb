class ReceiptLegalCode
  attr_accessor(:letter, :place, :instance)
  
  def initialize(letter, place, instance)
    self.letter = letter
    self.place = place
    self.instance = instance
  end

  class << self
    def default
      new("C", 2, 1)
    end
  end
end
