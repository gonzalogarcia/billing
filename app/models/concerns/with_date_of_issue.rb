module WithDateOfIssue
  def between(from, up_to)
    where("date_of_issue >= (?)", from)
      .where("date_of_issue <= (?)", up_to)
  end
end
