class PartnerCategory < ActiveRecord::Base
  validates_presence_of(:price, :description)
  has_many(:partners, foreign_key: :actual_partner_category_id)
  has_many(:seminary_category_prices)
  has_many(:quotas)
  has_many(:partners_quotas, through: :partners, source: :quotas)

  after_save(:update_upcoming_quotas)

  def partners_count
    partners.count
  end

  def update_upcoming_quotas
    quotas.upcoming.each(&:update_price)
  end

  def total_overdue_quotas_count
    partners_overdue_quotas.count
  end

  def total_overdue_quotas_pending_amount
    partners_overdue_quotas.map(&:pending_amount).sum
  end

  def partners_overdue_quotas
    partners_quotas.overdue
  end

  def paid?
    self.price > 0
  end

  class << self
    def paid
      where("price > 0")
    end
  end
end
