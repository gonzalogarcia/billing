class Client < ActiveRecord::Base
  validates_presence_of(:first_name, :last_name)
  has_many(:receipts,  -> { order(:date_of_issue) }, dependent: :destroy)
  before_save(:default_attrs)

  def default_attrs
    self.address ||= ''
    self.place ||= ''
    self.postal_code ||= ''
    self.province ||= ''
  end

  def full_name
    "#{last_name} #{first_name}"
  end

  def seminary_offers
    Seminary.all.map do |seminary|
      offer_to(seminary)
    end
  end

  def offer_to(seminary)
    seminary.client_offer
  end
end
