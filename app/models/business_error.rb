class BusinessError < StandardError
  extend ActiveModel::Naming
  extend ActiveModel::Translation

  def initialize(message = nil)
    @message = message
  end

  def message
    @message.nil? ? self.class.model_name.human : @message
  end

  def to_s
    message
  end
end
