class Seminary < ActiveRecord::Base
  validates_presence_of(:default_price, :title)
  has_many(:seminary_category_prices)

  def held_on_description
    held_on ? held_on.in_time_zone(-3).strftime("%Y/%m/%d %H:%M") : ''
  end

  def client_offer
    SeminaryOffer.new(self, default_price);
  end

  def partner_offer(partner)
    SeminaryOffer.new(
      self,
      price_for(partner))
  end

  def price_for(partner)
    seminary_category_price = seminary_category_prices.find_by(partner_category: partner.actual_partner_category)
    seminary_category_price.present? ? seminary_category_price.price : default_price
  end

  def not_defined_categories
    PartnerCategory.
      where("partner_categories.id NOT IN (?)", seminary_category_prices.pluck(:partner_category_id))
  end

  class << self
    def by_held_on
      order(:held_on)
    end
  end
end
