class SeminaryOffer
  attr_accessor(:seminary, :price)
  delegate(:title, :held_on, :held_on_description, to: :seminary)

  def initialize(seminary, price)
    self.seminary = seminary
    self.price = price
  end

  def seminary_id
    seminary.id
  end

  def description
    title
  end
end
