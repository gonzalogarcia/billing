class Quota < ActiveRecord::Base
  belongs_to(:partner_category)
  belongs_to(:partner, class_name: Partner.name)
  has_many(:receipt_lines, dependent: :destroy)

  validates_presence_of(:month, :year, :partner_category, :partner, :price, :paid_amount)
  validates_uniqueness_of(:month, scope: [:year, :partner_id])

  before_validation :remember_price, on: :create
  after_create :update_partner
  before_validation :set_paid_amount

  def change_category(new_category)
    self.partner_category = new_category
    self.remember_price
    self.save!
  end

  def update_partner
    partner.quota_created(year, month)
  end

  def remember_price
    self.price = partner_category.price
  end

  def pending_amount
    price - paid_amount
  end

  def completely_paid
    pending_amount <= 0
  end

  def update_paid_amount
    set_paid_amount
    save!
  end

  def update_price
    update_attributes(price: partner_category.price) if paid_amount <= 0
  end

  def set_paid_amount
    self.paid_amount = receipt_lines.pluck(:amount).sum || 0
  end

  def next
    if month == 12
      next_quota_month = 1
      next_quota_year = year.next
    else
      next_quota_month = month.next
      next_quota_year = year
    end

    Quota.find_or_create_by(
      year: next_quota_year, 
      month: next_quota_month,
      partner: partner,
      partner_category: partner.actual_partner_category)
  end

  class << self
    def paid
      where("quota.paid_amount >= quota.price")
    end

    def unpaid
      where("quota.paid_amount < quota.price")
    end

    def completely_unpaid
      where(paid_amount: 0)
    end

    def overdue
      started.unpaid
    end

    def started
      where("
        (quota.year < ?) OR 
        ((quota.year = ?) AND (quota.month <= ?))", 
        Time.now.year, Time.now.year, Time.now.month)
    end

    def completed
      where("
        (quota.year < ?) OR 
        ((quota.year = ?) AND (quota.month < ?))", 
        Time.now.year, Time.now.year, Time.now.month)
    end

    def upcoming
      where("
        (quota.year > ?) OR 
        ((quota.year = ?) AND (quota.month > ?))", 
        Time.now.year, Time.now.year, Time.now.month)
    end

    def by_period
      order(:year, :month)
    end
  end
end
