class ReceiptsReport
  attr_accessor(:from, :up_to)

  def initialize(params)
    self.from = params[:from]
    self.up_to = params[:up_to]
  end

  def receipts
    @receipts ||= Receipt.between(from, up_to).order(:date_of_issue)
  end

  def amount
    receipts.map(&:full_amount).sum
  end
end
