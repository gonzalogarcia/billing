class OverdueQuotasReport
  attr_accessor(:minium_quotas_count)

  def initialize(minium_quotas_count)
    self.minium_quotas_count = minium_quotas_count
  end 

  def categories_overdue_quotas
    @categories_overdue_quotas ||= PartnerCategory.paid.map{ |c| CategoryOverdueQuotas.new(c, minium_quotas_count) }
  end

  def count
    categories_overdue_quotas.map(&:count).sum
  end

  def amount
    categories_overdue_quotas.map(&:amount).sum
  end
end
