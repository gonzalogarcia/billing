json.extract!(seminary_category_price, 
  :seminary_id, 
  :partner_category_id, 
  :price, 
  :partner_category_description,
  :seminary_title)
