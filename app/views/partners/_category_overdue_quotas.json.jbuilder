json.extract!(
  category_overdue_quotas,
  :category_id,
  :category_description,
  :minium_quotas_count,
  :count,
  :amount)
json.partners do 
  json.array!(category_overdue_quotas.partners, partial: 'partners/partner', as: :partner)
end
