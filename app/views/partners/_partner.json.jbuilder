json.extract!(partner, 
  :id, 
  :first_name, 
  :last_name,
  :full_name,
  :overdue_quotas_count,
  :overdue_quotas_amount,
  :last_paid_quota,
  :email,
  :address,
  :place,
  :postal_code,
  :province,
  :actual_partner_category_id,
  :cuit,
  :paying,
  :identificator)
json.actual_partner_category do
  json.partial!('partner_categories/simple_partner_category', partner_category: partner.actual_partner_category)
end 
