json.extract!(@overdue_quotas_report, 
  :minium_quotas_count,
  :count,
  :amount)
json.categories_overdue_quotas do
  json.array!(
    @overdue_quotas_report.categories_overdue_quotas,
    partial: 'partners/category_overdue_quotas', 
    as: :category_overdue_quotas)
end
