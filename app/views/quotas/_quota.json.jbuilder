json.extract!(quota, :id, :month, :year, :completely_paid, :pending_amount, :price, :paid_amount)
json.partner_category do
  json.partial!('partner_categories/partner_category', partner_category: quota.partner_category)
end
