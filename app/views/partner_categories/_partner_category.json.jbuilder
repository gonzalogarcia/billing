json.extract!(partner_category, 
  :id, 
  :description, 
  :price, 
  :partners_count,
  :total_overdue_quotas_count,
  :total_overdue_quotas_pending_amount)
