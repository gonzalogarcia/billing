json.extract!(report, :from, :up_to, :amount)
json.receipts do
  json.partial!('receipts/receipts', receipts: report.receipts)
end
