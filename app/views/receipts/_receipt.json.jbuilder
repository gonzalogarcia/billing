json.extract!(receipt, 
  :id, 
  :client_id,
  :full_amount,
  :date_of_issue_description,
  :date_of_issue_simple_description,
  :client_address,
  :client_place,
  :client_postal_code,
  :client_place_with_postal_code,
  :client_province,
  :client_full_name,
  :client_cuit,
  :legal_code_description,
  :payment_methods_amounts_description)
json.receipt_lines do
  json.array!(receipt.receipt_lines, partial: 'receipt_lines/receipt_line', as: :receipt_line)
end
