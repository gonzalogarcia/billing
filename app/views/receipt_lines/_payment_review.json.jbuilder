json.extract!(review, :from, :up_to, :payment_method_description, :size, :full_amount)
json.lines do
  json.array!(review.lines, partial: 'receipt_lines/receipt_line', as: :receipt_line)
end
