json.extract!(review, :full_amount)
json.partner_category do
  json.partial!('partner_categories/partner_category', partner_category: review.partner_category)
end