json.extract!(@review_by_payments, :from, :up_to, :size, :full_amount, :quota_amount, :others_amount)
json.reviews do
  json.array!(@review_by_payments.reviews, partial: 'receipt_lines/payment_review', as: :review)
end
json.partner_category_reviews do
  json.array!(@review_by_payments.partner_categories_reviews, partial: 'receipt_lines/partner_category_review', as: :review)
end

