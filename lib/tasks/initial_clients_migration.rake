require "simple-spreadsheet"

namespace :initial_clients_migration do
  task create_clients: :environment do 
    s = SimpleSpreadsheet::Workbook.read("db/billing_clientes.xls")
    s.selected_sheet = s.sheets.first
    s.first_row.upto(s.last_row) do |line|
      last_name = s.cell(line, 2).split.first
      first_name = s.cell(line, 2).split[1..-1].join(' ')

      partner = Partner.create!(
        first_name: first_name.blank? ? 'Nombre' : first_name,
        last_name: last_name.blank? ? 'Apellido' : last_name,
        address: s.cell(line, 3) || 'dirección',
        place: s.cell(line, 4) || 'localidad',
        postal_code: s.cell(line, 5) || 'código postal',
        province: s.cell(line, 6) || 'provincia',
        telephone_number: s.cell(line, 7),
        cuit: s.cell(line, 8),
        actual_partner_category: 
            PartnerCategory.find_by(description: s.cell(line, 9)) || 
            PartnerCategory.find_by(description: 'Cliente no socio'))

      first_overdue_year = s.cell(line, 10) || Time.now.year
      first_overdue_month = s.cell(line, 11) || Time.now.month

      partner.quotas.destroy_all
      partner.quotas.create!(
        year: first_overdue_year, 
        month: first_overdue_month, 
        partner_category: partner.actual_partner_category)
      partner.generate_quotas
    end
  end
end
