require "simple-spreadsheet"

namespace :add_indocator_to_initial_clients do
  task edit_clients: :environment do 
    s = SimpleSpreadsheet::Workbook.read("db/billing_clientes.xls")
    s.selected_sheet = s.sheets.first
    s.first_row.upto(s.last_row) do |line|
      last_name = s.cell(line, 2).split.first
      first_name = s.cell(line, 2).split[1..-1].join(' ')
      cuit = s.cell(line, 8)
      identificator = s.cell(line, 1)

      partner = Partner.find_by(first_name: first_name, last_name: last_name)

      partner.update_attributes(identificator: identificator) if partner
    end
  end
end
