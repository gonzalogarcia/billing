Rails.application.routes.draw do
  get 'receipt_lines/index'

  root to: "sessions#create"

  resources :users

  resources :sessions, only: [:create]
  delete 'sessions', to: "sessions#destroy"

  get 'quotas/:id/next', to: 'quotas#next'

  resources :partners, only: [:index, :show, :create, :update] do
    resource :quotas, only: :show, to: "partners#quotas"
    member do
      get :coming_quota
      get :receipts
    end
  end
  get 'overdue_quotas_report', to: "partners#overdue_quotas_report"

  get 'clients/:id/seminary_offers', to: 'clients#seminary_offers'
  resources :seminaries, only: [:index, :create, :update, :show] do
    resources :seminary_category_prices, only: [:index, :create]
    get :not_defined_categories
  end

  resources :partner_categories, only: [:index, :show, :create, :update] do
    resource :partners, only: :show, to: "partner_categories#partners"
  end

  resources :receipts, only: [:index, :show, :create, :destroy]
  get 'receipts_report', to: 'receipts#report'
  resources :receipt_lines, only: :index
  get 'receipt_lines/each_payment_review', to: 'receipt_lines#each_payment_review'

  get 'next_receipt_legal_code', to: "receipts#next_legal_code"
  get 'payment_methods', to: "receipts#payment_methods"
end
