FactoryGirl.define do
  factory :client do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.email }
    address { Faker::Address.street_address }
    postal_code { Faker::Address.postcode }
    place { Faker::Address.city }
    province { Faker::Address.state }
    factory :partner, class: Partner.name do
      actual_partner_category { create(:partner_category) }
    end
  end
end
