FactoryGirl.define do
  factory :partner_category do
    price { rand(1..99999) }
    description { Faker::Lorem.sentence } 
  end
end
