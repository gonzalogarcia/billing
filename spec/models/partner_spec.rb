require 'rails_helper'
require 'rspec-rails'

describe Partner do
  describe 'generate_quotas' do
    context "when the last quota is from many years ago" do
      let!(:partner){ create(:partner) }
      before do
        Timecop.travel(5.years.from_now + 8.months)
      end

      it 'should generate for all the months on all the years' do
        Partner.generate_quotas
        expect(partner.quotas.size).to eq(69)
        expect(partner.quotas.last.month).to eq(Time.now.month)
        expect(partner.quotas.last.year).to eq(Time.now.year)
      end
    end

    context "when the last quota is from last year" do
      let!(:partner){ create(:partner) }
      before do
        Timecop.travel(Time.now + 1.year)
      end

      it "should complete this year and start the next one" do
        Partner.generate_quotas
        expect(partner.quotas.size).to eq(13)
        expect{ partner.generate_quotas }.not_to change{ partner.quotas.size }
      end
    end
  end
end
