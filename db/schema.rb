# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151117021958) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clients", force: true do |t|
    t.string   "type"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "actual_partner_category_id"
    t.integer  "status",                     default: 0
    t.integer  "last_quota_year"
    t.integer  "last_quota_month"
    t.string   "address"
    t.string   "place"
    t.string   "postal_code"
    t.string   "province"
    t.string   "telephone_number"
    t.string   "dni"
    t.string   "cuit"
    t.integer  "identificator"
  end

  add_index "clients", ["actual_partner_category_id"], name: "index_clients_on_actual_partner_category_id", using: :btree
  add_index "clients", ["email"], name: "index_clients_on_email", using: :btree
  add_index "clients", ["identificator"], name: "index_clients_on_identificator", using: :btree
  add_index "clients", ["last_name"], name: "index_clients_on_last_name", using: :btree
  add_index "clients", ["last_quota_month"], name: "index_clients_on_last_quota_month", using: :btree
  add_index "clients", ["last_quota_year"], name: "index_clients_on_last_quota_year", using: :btree
  add_index "clients", ["status"], name: "index_clients_on_status", using: :btree

  create_table "partner_categories", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "price"
    t.string   "description"
  end

  create_table "quota", force: true do |t|
    t.integer  "price"
    t.integer  "month"
    t.integer  "year"
    t.integer  "partner_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "paid_amount",         default: 0
    t.integer  "partner_id"
  end

  add_index "quota", ["month"], name: "index_quota_on_month", using: :btree
  add_index "quota", ["partner_category_id"], name: "index_quota_on_partner_category_id", using: :btree
  add_index "quota", ["partner_id"], name: "index_quota_on_partner_id", using: :btree
  add_index "quota", ["year"], name: "index_quota_on_year", using: :btree

  create_table "receipt_lines", force: true do |t|
    t.integer  "receipt_id"
    t.string   "type"
    t.integer  "position"
    t.string   "description"
    t.integer  "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quota_id"
    t.integer  "seminary_id"
    t.integer  "payment_method"
  end

  add_index "receipt_lines", ["payment_method"], name: "index_receipt_lines_on_payment_method", using: :btree
  add_index "receipt_lines", ["position"], name: "index_receipt_lines_on_position", using: :btree
  add_index "receipt_lines", ["quota_id"], name: "index_receipt_lines_on_quota_id", using: :btree
  add_index "receipt_lines", ["receipt_id"], name: "index_receipt_lines_on_receipt_id", using: :btree
  add_index "receipt_lines", ["seminary_id"], name: "index_receipt_lines_on_seminary_id", using: :btree

  create_table "receipts", force: true do |t|
    t.integer  "client_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "date_of_issue"
    t.string   "client_type"
    t.integer  "full_amount"
    t.string   "legal_letter"
    t.integer  "legal_place_code"
    t.integer  "legal_instance_code", limit: 8
    t.string   "client_address"
    t.string   "client_place"
    t.string   "client_postal_code"
    t.string   "client_province"
    t.string   "client_full_name"
    t.string   "client_cuit"
  end

  add_index "receipts", ["client_id"], name: "index_receipts_on_client_id", using: :btree
  add_index "receipts", ["client_type"], name: "index_receipts_on_client_type", using: :btree
  add_index "receipts", ["date_of_issue"], name: "index_receipts_on_date_of_issue", using: :btree
  add_index "receipts", ["full_amount"], name: "index_receipts_on_full_amount", using: :btree
  add_index "receipts", ["legal_instance_code"], name: "index_receipts_on_legal_instance_code", using: :btree
  add_index "receipts", ["legal_place_code"], name: "index_receipts_on_legal_place_code", using: :btree

  create_table "seminaries", force: true do |t|
    t.datetime "held_on"
    t.string   "title"
    t.integer  "default_price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "seminaries", ["held_on"], name: "index_seminaries_on_held_on", using: :btree
  add_index "seminaries", ["title"], name: "index_seminaries_on_title", using: :btree

  create_table "seminary_category_prices", force: true do |t|
    t.integer  "seminary_id"
    t.integer  "partner_category_id"
    t.integer  "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "seminary_category_prices", ["partner_category_id"], name: "index_seminary_category_prices_on_partner_category_id", using: :btree
  add_index "seminary_category_prices", ["seminary_id"], name: "index_seminary_category_prices_on_seminary_id", using: :btree

  create_table "users", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "username"
    t.string   "type"
  end

  add_index "users", ["username"], name: "index_users_on_username", using: :btree

end
