class AddPartnerToQuota < ActiveRecord::Migration
  def change
    add_column :quota, :partner_id, :integer
    add_index :quota, :partner_id
  end
end
