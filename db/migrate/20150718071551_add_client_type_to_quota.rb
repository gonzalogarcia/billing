class AddClientTypeToQuota < ActiveRecord::Migration
  def change
    add_column :quota, :client_type, :string
  end
end
