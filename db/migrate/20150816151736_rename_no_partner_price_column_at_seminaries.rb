class RenameNoPartnerPriceColumnAtSeminaries < ActiveRecord::Migration
  def change
    rename_column(:seminaries, :no_partner_price, :default_price)
  end
end
