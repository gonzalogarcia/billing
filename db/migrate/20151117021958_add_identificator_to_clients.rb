class AddIdentificatorToClients < ActiveRecord::Migration
  def change
    add_column :clients, :identificator, :integer
    add_index :clients, :identificator
  end
end
