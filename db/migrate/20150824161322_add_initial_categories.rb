class AddInitialCategories < ActiveRecord::Migration
  def change
    PartnerCategory.create!(description: 'ADHERENTE', price: 550)
    PartnerCategory.create!(description: 'MIEMBRO', price: 700)
    PartnerCategory.create!(description: 'A. M. E.', price: 950)
    PartnerCategory.create!(description: 'Cliente no socio', price: 0)
    PartnerCategory.create!(description: 'Socio dado de baja', price: 0)
  end
end
