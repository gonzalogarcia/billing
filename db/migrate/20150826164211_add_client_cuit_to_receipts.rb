class AddClientCuitToReceipts < ActiveRecord::Migration
  def change
    add_column :receipts, :client_cuit, :string
  end
end
