class AddPartnerCategoryToPartners < ActiveRecord::Migration
  def change
    add_reference :clients, :partner_category, index: true
  end
end
