class CreateSeminaries < ActiveRecord::Migration
  def change
    create_table :seminaries do |t|
      t.datetime :held_on
      t.string :title
      t.integer :no_partner_price

      t.timestamps
    end
    add_index :seminaries, :held_on
    add_index :seminaries, :title
  end
end
