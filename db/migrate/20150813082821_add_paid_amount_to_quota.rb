class AddPaidAmountToQuota < ActiveRecord::Migration
  def change
    add_column :quota, :paid_amount, :integer, default: 0
  end
end
