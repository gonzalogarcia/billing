class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :type
      t.string :first_name
      t.string :last_name
      t.string :email

      t.timestamps
    end
    add_index :clients, :last_name
    add_index :clients, :email
  end
end
