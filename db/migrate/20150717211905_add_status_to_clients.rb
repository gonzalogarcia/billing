class AddStatusToClients < ActiveRecord::Migration
  def change
    add_column :clients, :status, :integer
    add_index :clients, :status
  end
end
