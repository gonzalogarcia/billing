class AddSeminaryToSeminaryLines < ActiveRecord::Migration
  def change
    add_reference :receipt_lines, :seminary, index: true
  end
end
