class CreateReceiptLines < ActiveRecord::Migration
  def change
    create_table :receipt_lines do |t|
      t.references :receipt, index: true
      t.string :type
      t.integer :position
      t.string :description
      t.integer :amount

      t.timestamps
    end
    add_index :receipt_lines, :position
  end
end
