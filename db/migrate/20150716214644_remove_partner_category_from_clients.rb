class RemovePartnerCategoryFromClients < ActiveRecord::Migration
  def change
    remove_column :clients, :partner_category_id, :integer
  end
end
