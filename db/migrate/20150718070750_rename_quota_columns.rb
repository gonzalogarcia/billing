class RenameQuotaColumns < ActiveRecord::Migration
  def change
    rename_column(:quota, :partner_id, :client_id)
  end
end
