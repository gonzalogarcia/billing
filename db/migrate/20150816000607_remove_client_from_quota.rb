class RemoveClientFromQuota < ActiveRecord::Migration
  def change
    remove_column :quota, :client_id
    remove_column :quota, :client_type
  end
end
