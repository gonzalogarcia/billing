class AddCachesToClients < ActiveRecord::Migration
  def change
    add_column :clients, :last_quota_year, :integer
    add_index :clients, :last_quota_year
    add_column :clients, :last_quota_month, :integer
    add_index :clients, :last_quota_month
  end
end
