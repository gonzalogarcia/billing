class AddLegalNumberToReceipts < ActiveRecord::Migration
  def change
    add_column :receipts, :legal_letter, :string
    add_column :receipts, :legal_place_code, :integer
    add_index :receipts, :legal_place_code
    add_column :receipts, :legal_instance_code, :integer, limit: 8
    add_index :receipts, :legal_instance_code
  end
end
