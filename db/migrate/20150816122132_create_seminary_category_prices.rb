class CreateSeminaryCategoryPrices < ActiveRecord::Migration
  def change
    create_table :seminary_category_prices do |t|
      t.references :seminary, index: true
      t.references :category, index: true
      t.integer :price

      t.timestamps
    end
  end
end
