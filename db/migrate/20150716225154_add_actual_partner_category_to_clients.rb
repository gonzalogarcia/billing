class AddActualPartnerCategoryToClients < ActiveRecord::Migration
  def change
    add_column :clients, :actual_partner_category_id, :integer
    add_index :clients, :actual_partner_category_id
  end
end
