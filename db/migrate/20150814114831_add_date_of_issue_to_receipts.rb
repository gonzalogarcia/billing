class AddDateOfIssueToReceipts < ActiveRecord::Migration
  def change
    add_column :receipts, :date_of_issue, :datetime
    add_index :receipts, :date_of_issue
  end
end
