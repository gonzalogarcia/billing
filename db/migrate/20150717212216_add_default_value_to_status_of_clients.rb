class AddDefaultValueToStatusOfClients < ActiveRecord::Migration
  def change
    change_column :clients, :status, :integer, default: 0
  end
end
