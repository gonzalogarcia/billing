class AddDniToClients < ActiveRecord::Migration
  def change
    add_column :clients, :dni, :string
  end
end
