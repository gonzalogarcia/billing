class AddClientFullNameToReceipts < ActiveRecord::Migration
  def change
    add_column :receipts, :client_full_name, :string
  end
end
