class AddPriceAndDescriptionToPartnerCategories < ActiveRecord::Migration
  def change
    add_column :partner_categories, :price, :integer
    add_column :partner_categories, :description, :string
  end
end
