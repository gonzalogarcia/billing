class AddLocationDataToClients < ActiveRecord::Migration
  def change
    add_column :clients, :adress, :string
    add_column :clients, :place, :string
    add_column :clients, :postal_code, :string
    add_column :clients, :province, :string
  end
end
