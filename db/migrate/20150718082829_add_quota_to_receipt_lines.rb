class AddQuotaToReceiptLines < ActiveRecord::Migration
  def change
    add_reference :receipt_lines, :quota, index: true
  end
end
