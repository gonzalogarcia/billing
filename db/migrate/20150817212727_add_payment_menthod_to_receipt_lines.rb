class AddPaymentMenthodToReceiptLines < ActiveRecord::Migration
  def change
    add_column :receipt_lines, :payment_method, :integer
    add_index :receipt_lines, :payment_method
  end
end
