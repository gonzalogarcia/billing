class AddFullAmountToReceipts < ActiveRecord::Migration
  def change
    add_column :receipts, :full_amount, :integer
    add_index :receipts, :full_amount
  end
end
