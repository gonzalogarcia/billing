class NewPrices < ActiveRecord::Migration
  def change
  	PartnerCategory.find_by(description: 'ADHERENTE').update_attributes!(price: 700)
    PartnerCategory.find_by(description: 'MIEMBRO').update_attributes!(price: 800)
    PartnerCategory.find_by(description: 'A. M. E.').update_attributes!(price: 1050)
  end
end
