class AddTelephoneNumberToClients < ActiveRecord::Migration
  def change
    add_column :clients, :telephone_number, :string
  end
end
