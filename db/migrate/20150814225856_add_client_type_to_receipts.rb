class AddClientTypeToReceipts < ActiveRecord::Migration
  def change
    add_column :receipts, :client_type, :string
    add_index :receipts, :client_type
  end
end
