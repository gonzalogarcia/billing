class CreateQuota < ActiveRecord::Migration
  def change
    create_table :quota do |t|
      t.integer :partner_id
      t.integer :price
      t.integer :month
      t.integer :year
      t.references :partner_category, index: true

      t.timestamps
    end
    add_index :quota, :partner_id
    add_index :quota, :month
    add_index :quota, :year
  end
end
