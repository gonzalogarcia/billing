class RenameCategoryColumnAtSeminaryCategoryPrices < ActiveRecord::Migration
  def change
    rename_column(:seminary_category_prices, :category_id, :partner_category_id)
  end
end
