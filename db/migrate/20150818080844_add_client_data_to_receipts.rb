class AddClientDataToReceipts < ActiveRecord::Migration
  def change
    add_column :receipts, :client_address, :string
    add_column :receipts, :client_place, :string
    add_column :receipts, :client_postal_code, :string
    add_column :receipts, :client_province, :string
  end
end
